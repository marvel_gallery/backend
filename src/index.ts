import { ErrorMs, Microservice, MsManager, ConfService } from '@coppel/microservices';
import * as mongoose from 'mongoose';
import MarvelCharacter from './schemas/marvel_service';
import MarvelLogin from './schemas/marvel_login';

const options = {
	useNewUrlParser: true,
	autoIndex: false, 
	reconnectTries: 3,
	reconnectInterval: 500,
	poolSize: 10,
    connectTimeoutMS: 10000,
    useUnifiedTopology: true
};


@MsManager.Define({
    App: 'planlealtad',
    Broker: ['10.27.113.125:9092'],
    Debug: false,
    Name: 'marvel_service',
    Version: 'v1',
})
class marvel_service extends Microservice {

    private db: mongoose.Connection;

    constructor(conf: ConfService) {
      super(conf)

      const conn: string = process.env.GCD_MONGO ? process.env.GCD_MONGO : 'mongodb://planlealtad:HhrM8I0@10.28.114.76:27017/planlealtadpruebas?authSource=admin';
      mongoose.connect(conn, options);      
      this.db = mongoose.connection;
      this.db.once('open', () => {
        console.log('Conectado a mongo');
      });
    }

    @MsManager.Errors()
    public errores(): ErrorMs {
        return {
            '-12': 'Error definido por el usuario',
            '-2': 'Datos Incorrectos',
            '-1': 'Dato no encontrado'
        };
    }

    /**
     * Metodo de smoketest
     */
    public smoketest(): boolean | Promise<boolean> {
        // Retornar la promesa
        return new Promise( ( rr: (r: boolean) => void, ff: (r: boolean) => void ) => {
            rr(true);
        });
    }

    @MsManager.Create()
    public marvel(@MsManager.ctx('data') data: any): any {

        return new Promise(async (rr, ff) => {

            //Busca los personajes leidos por usuarios
            if(data.opcion == 1){
                var parametros: {[key: string]: any} = {};

                if(data.id_character >= 0){
                    parametros.id_character = data.id_character;
                }

                if(data.is_read >= 0){
                    parametros.is_read = data.is_read;
                }

                if(data.user){
                    parametros.user = data.user;
                }
                
                let character = await MarvelCharacter.find(parametros);
                return rr(character);
            }

            //Guarda los personajes leidos y el usuario
            if(data.opcion == 2){
                var parametros: {[key: string]: any} = {};
                
                if(data.id_character >= 0){
                    parametros.id_character = data.id_character;
                }

                if(data.is_read >= 0){
                    parametros.is_read = data.is_read;
                }

                if(data.user){
                    parametros.user = data.user;
                }

                let characterfind = await MarvelCharacter.find(parametros);
                if(characterfind.length != 0){
                    return rr(characterfind);
                }
                
                let character = await MarvelCharacter.create(parametros);
                return rr(character);
            }

            //Consulta los usuarios para el login
            if(data.opcion == 3){
                var parametros: {[key: string]: any} = {};
                
                if(data.user){
                    parametros.user = data.user;
                }

                if(data.pass){
                    parametros.pass = data.pass;
                }
                let loginfind = await MarvelLogin.find(parametros);
                if(loginfind.length == 0){
                    return ff( this.emitError(-1));
                }else{
                    return rr(loginfind);
                }

            }

            //Guarda los personajes leidos al estar deslogueado
            //Valida que al guardar los personajes no existan en tu catalogo
            if(data.opcion == 4){
                let characterfind = await MarvelCharacter.findOne({"user":data.user,"id_character":data.id_character});

                if(characterfind == null){
                    let update = await MarvelCharacter.findOneAndUpdate({"user":"anonimo"}, {$set:{"user":data.user,"id_character":data.id_character}});
                    return rr(update);
                }else if (characterfind.id_character == data.id_character){
                    let remove = await MarvelCharacter.findOneAndRemove({"user":"anonimo","id_character":data.id_character});
                    return rr(remove);
                }
            }

    });
}

    @MsManager.Listener()
    public global(@MsManager.ctx('data') data: any): any {
        // return data.uno + data.dos;
        return { method: 'Listener34' };
        // throw this.emitError(-12);
    }

}
