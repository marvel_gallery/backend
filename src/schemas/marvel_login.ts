import * as mongoose from 'mongoose'; 
mongoose.pluralize(null)


export interface marvellogin extends mongoose.Document {
    user:String;
    pass:String;
}

const marvelloginSchema = new mongoose.Schema({
    user:{type: String,required: false,default: ""},
    pass:{type: String,required: false}

},{collection:'marvel_login' });


export default   mongoose.model<marvellogin>('marvel_login', marvelloginSchema);