import * as mongoose from 'mongoose'; 
mongoose.pluralize(null)


export interface marvelservice extends mongoose.Document {
    user:String;
    id_character: Number;
    is_read: Boolean;
}

const marvelserviceSchema = new mongoose.Schema({
    user:{type: String,required: false,default: "anonimo"},
    id_character:{type: Number,required: true},
    is_read:{type: Boolean,required: false,default:true}
},{collection:'marvel_service' });


export default   mongoose.model<marvelservice>('marvel_service', marvelserviceSchema);