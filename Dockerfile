FROM mhart/alpine-node:12
WORKDIR /app
COPY . ./
RUN apk add --no-cache python make g++ && \
    python -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip install --upgrade pip setuptools && \
    rm -vrf /var/cache/apk/*
RUN npm i -g typescript && npm i && npm run build-ts

FROM mhart/alpine-node:slim-12
WORKDIR /app
COPY --from=0 /app .
RUN apk add --no-cache dumb-init  && \
    rm -vrf /var/cache/apk/*
ENTRYPOINT ["dumb-init", "--"]
CMD ["node", "dist/index.js"]